'use strict';

const Hapi = require('hapi');
const config = require('./src/configs/config');

// Create a server with a host and port
const server = new Hapi.Server();
server.connection(config.hapi);

server.register([
    require('inert'),
    require('vision'),
    {
        register: require('hapi-mongodb'),
        options: config.mongo
    },
    {
        'register': require('hapi-swagger'),
        'options': config.swagger
    }
], function (err) {
    if (err) {
        throw err;
    }

    // Add the route
    server.route(require('./src/routes'));

    // Start the server
    server.start(function(err) {

        if (err) {
            throw err;
        }
        console.log('Server running at:', server.info.uri);
    });
});
