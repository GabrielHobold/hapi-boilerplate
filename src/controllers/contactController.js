'use strict';

const ReplyHelper = require('../libs/ReplyHelper');
const ContactDAO = require('../dao/ContactDAO');

/**
* Controller para a rota de Contatos
*/
class ContactController {

    /**
    * Busca os Contatos
    * @param  {object} request Requisição
    * @param  {object} reply   Resposta
    */
    getAll(request, reply) {
        const mongo = request.server.plugins['hapi-mongodb'];
        const dao = new ContactDAO(mongo);
        const helper = new ReplyHelper(reply);

        return helper.find(dao.findAll({}));
    }

    /**
    * Busca um Contato
    * @param  {object} request Requisição
    * @param  {object} reply   Resposta
    */
    getOne(request, reply) {
        const mongo = request.server.plugins['hapi-mongodb'];
        const dao = new ContactDAO(mongo);
        const helper = new ReplyHelper(reply);

        return helper.findOne(dao.findOneById(request.params.id));
    }

    /**
    * Cria um Contato
    * @param  {object} request Requisição
    * @param  {object} reply   Resposta
    */
    create(request, reply) {
        const mongo = request.server.plugins['hapi-mongodb'];
        const dao = new ContactDAO(mongo);
        const helper = new ReplyHelper(reply);

        dao.findOneByParams({email: request.payload.email}).then((contact) => {
            if(contact) {
                return helper.validationError('E-mail já cadastrado');
            }
            return helper.created(dao.insert(request.payload));
        })
        .catch(err => helper.serverError(err));
    }

    /**
    * Apaga um Contato
    * @param  {object} request Requisição
    * @param  {object} reply   Resposta
    */
    delete(request, reply) {
        const mongo = request.server.plugins['hapi-mongodb'];
        const dao = new ContactDAO(mongo);
        const helper = new ReplyHelper(reply);

        return helper.deleted(dao.deleteOneById(request.params.id));
    }

    /**
    * Atualiza um Contato
    * @param  {object} request Requisição
    * @param  {object} reply   Resposta
    */
    update(request, reply) {
        const mongo = request.server.plugins['hapi-mongodb'];
        const dao = new ContactDAO(mongo);
        const helper = new ReplyHelper(reply);

        return helper.updated(dao.updateOneById(request.params.id, request.payload));
    }
}

module.exports = new ContactController();
