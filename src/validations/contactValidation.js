'use strict';

const Joi = require('joi');

module.exports = {
    getOne: {
        params: {
            id: Joi.string().alphanum().min(24).max(24).required()
        }
    },

    create: {
        payload: {
            name: Joi.string().required(),
            email: Joi.string().email().required(),
            phone: Joi.string(),
            age: Joi.number().integer().min(1),
        }
    },

    delete: {
        params: {
            id: Joi.string().alphanum().min(24).max(24).required()
        }
    },

    update: {
        params: {
            id: Joi.string().alphanum().min(24).max(24).required()
        },
        payload: {
            name: Joi.string().required(),
            email: Joi.string().email().required(),
            phone: Joi.string(),
            age: Joi.number().integer().min(1),
        }
    }
};
