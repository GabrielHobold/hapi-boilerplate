'use strict';

/**
 * DAO Base para a aplicação
 */
class DAO {

    /**
     * Construtor do DAO
     * @param  {object} mongo Instância do mongo
     */
    constructor(mongo) {
        this.db = mongo.db;
        this.ObjectID = mongo.ObjectID;
    }

    /**
     * Busca todos os documentos da collection
     * @param  {object}   params   Parâmetros de busca
     * @param  {Function} callback
     */
    findAll(params, callback) {
        return this.collection.find(params).toArray(callback);
    }

    /**
     * Busca um documento da collection por ID
     * @param  {number}   id   Identificador do Documento
     * @param  {Function} callback
     */
    findOneById(id, callback) {
        return this.collection.findOne({ _id: new this.ObjectID(id) }, callback);
    }

    /**
     * Busca um documento por parâmetros
     * @param  {object}   params   Parâmetros de busca
     * @param  {Function} callback
     */
    findOneByParams(params, callback) {
        return this.collection.findOne(params, callback);
    }

    /**
     * Faz a inserção de um documento na collection
     * @param  {object}   data   Dados para inserir
     * @param  {Function} callback
     */
    insert(data, callback) {
        return this.collection.insertOne(data, callback);
    }

    /**
     * Atualiza um documento na collection por ID
     * @param  {number}   id   Identificador do Documento
     * @param  {object}   data   Dados para inserir
     * @param  {Function} callback
     */
    updateOneById(id, data, callback) {
        return this.collection.updateOne({ _id: new this.ObjectID(id) }, { $set: data }, callback);
    }

    /**
     * Remove um documento da collection por ID
     * @param  {number}   id   Identificador do Documento
     * @param  {Function} callback
     */
    deleteOneById(id, callback) {
        return this.collection.deleteOne({ _id: new this.ObjectID(id) }, callback);
    }
}

module.exports = DAO;
