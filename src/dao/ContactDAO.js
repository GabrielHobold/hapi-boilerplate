'use strict';

const DAO = require('./DAO');

/**
 * DAO do Contato
 */
class ContactDAO extends DAO {

    constructor(mongo) {
        super(mongo);
        this.collection = this.db.collection('contacts');
    }
}

module.exports = ContactDAO;
