'use strict';

const controller = require('../controllers/contactController');
const validator = require('../validations/contactValidation');

module.exports = [
    {
        method: 'GET',
        path: '/contacts',
        handler: controller.getAll,
        config: {
            description: 'Get all contacts',
            tags: ['api']
        }
    },
    {
        method: 'POST',
        path: '/contacts',
        handler: controller.create,
        config: {
            validate: validator.create,
            description: 'Create a contact',
            tags: ['api']
        }
    },
    {
        method: 'GET',
        path: '/contacts/{id}',
        handler: controller.getOne,
        config: {
            validate: validator.getOne,
            description: 'Get a contact',
            tags: ['api']
        }
    },
    {
        method: 'DELETE',
        path: '/contacts/{id}',
        handler: controller.delete,
        config: {
            validate: validator.delete,
            description: 'Delete a contact',
            tags: ['api']
        }
    },
    {
        method: 'PUT',
        path: '/contacts/{id}',
        handler: controller.update,
        config: {
            validate: validator.update,
            description: 'Update a contact',
            tags: ['api']
        }
    }
];
