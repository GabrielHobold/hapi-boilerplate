'use strict';

module.exports = {
    hapi: {
        host: 'localhost',
        port: 8000,
        routes: {
            cors: {
                origin: ['*']
            }
        }
    },

    mongo: {
        url: 'mongodb://127.0.0.1:27017/contacts',
        settings: {
            db: {

            }
        }
    },

    swagger: {
        info: {
            title: 'API Documentation',
            version: '1.0.0',
            description: 'DESCRIÇÃO DA API...',
            contact: {
                name: 'Gabriel Hobold',
                email: 'gabriel.hobolds@gmail.com'
            }
        },
        securityDefinitions: {
            'Bearer': {
                'type': 'apiKey',
                'name': 'Authorization',
                'in': 'header',
                'x-keyPrefix': 'Bearer '
            }
        }
    },
};
