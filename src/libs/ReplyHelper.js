'use strict';

const Boom = require('boom');

/**
* Classe para padronizar as respostas da API
*/
class ReplyHelper {
    /**
    * Construtor
    * @param  {function} reply Função de resposta do Hapi
    */
    constructor(reply) {
        this.reply = reply;
    }

    /**
    * Resposta padrão
    * @param  {object} promise  Resposta da requisição
    */
    general(promise) {
        return this.reply(promise);
    }

    /**
    * Resposta de um find all
    * @param  {object} promise  Resposta da requisição
    */
    find(promise) {
        return this.general(promise);
    }

    /**
    * Resposta de um find one
    * 	Responde como 404 (não encontrado se não houver erro e dados)
    * @param  {object} promise  Resposta da requisição
    */
    findOne(promise) {
        promise.then((data) => {
            if(!data) {
                return this.notFound();
            }
            return this.general(promise);
        })
        .catch(() => this.general(promise));
    }

    /**
    * Resposta de recurso criado
    * @param  {object} promise  Resposta da requisição
    */
    created(promise) {
        promise.then((data) => {
            return this.reply(null, data && data.ops ? data.ops[0] : null).code(201);
        })
        .catch(() => this.general(promise));
    }

    /**
    * Resposta de recurso atualizado
    * 	Responde como 404 (não encontrado se não houver erro e dados)
    * @param  {object} promise  Resposta da requisição
    */
    updated(promise) {
        return this.deleted(promise);
    }

    /**
    * Resposta de recurso removido
    * 	Responde como 404 (não encontrado se não houver erro e dados)
    * @param  {object} promise  Resposta da requisição
    */
    deleted(promise) {
        promise.then((data) => {
            if(!data || !data.result || !data.result.n) {
                return this.notFound();
            }
            return this.reply().code(204);
        })
        .catch(() => this.general(promise));
    }

    /**
    * Resposta para recurso não encontrado(404)
    * @param  {string} message  Mensagem
    */
    notFound(message) {
        return this.reply(Boom.notFound(message));
    }

    /**
    * Resposta para não autenticado(401)
    * @param  {string} message  Mensagem
    */
    unauthorized(message) {
        return this.reply(Boom.unauthorized(message));
    }

    /**
    * Resposta para erros de negócio(400)
    * @param  {string} message  Mensagem opcional
    * @param  {object} data  Objeto opcional de erro
    */
    validationError(message, data) {
        return this.reply(Boom.badRequest(message, data));
    }

    /**
    * Resposta para erros internos do sistema(500)
    * @param  {string} message  Mensagem opcional
    * @param  {object} data  Objeto opcional de erro
    */
    serverError(message, data) {
        return this.reply(Boom.internal(message, data));
    }
}

module.exports = ReplyHelper;
